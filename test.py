# TO RUN THE FILE : python test.py in cmdline
# linter and formatter - pylint and black
# DATA description:
# ID - an Id that represents a (Shop, Item) tuple within the test set
# shop_id - unique identifier of a shop
# item_id - unique identifier of a product
# item_category_id - unique identifier of item category
# date_block_num - a consecutive month number, used for convenience. January 2013 is 0, February 2013 is 1,..., October 2015 is 33
# date - date in format dd/mm/yyyy
# item_cnt_day - number of products sold. You are predicting a monthly amount of this measure
# item_price - current price of an item
# item_name - name of item
# shop_name - name of shop
# item_category_name - name of item category
# Understanding : Since this is a forecasting(time_series) problem we can by default have datetime features,
# lag based features and window based features for recency and seasonlity apart from other features that we can deduce from EDA
# MODULES TO BE INSTALLED IN THE ENV
# 1. Seaborn 2.matplotlib 3.memory_profiler


# imports
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from datetime import datetime
import time
# this import helps profile the code line by line when data is set to scale
#from memory_profiler import profile
# load the raw data


# @profile
def data_loader():
    # read_csv loads csv into a dataframe
    df_item_categories = pd.read_csv('item_categories.csv')
    df_items = pd.read_csv('items.csv')
    # set nrows to test with a reduced dataset
    df_sales_train = pd.read_csv('sales_train.csv')
    df_shops = pd.read_csv('shops.csv')
    df_test = pd.read_csv('test.csv')
    # merge all the files
    final_df = df_sales_train.join(df_items, on='item_id', rsuffix='_').join(df_shops, on='shop_id', rsuffix='_').join(
        df_item_categories, on='item_category_id', rsuffix='_')
    final_df.drop(['item_id_', 'shop_id_', 'item_category_id_'],
                  axis=1, inplace=True)

    # edge case check:check for duplicates the keep false option here spews the duplicates if any
    print("duplicates present after joins:",
          final_df.loc[final_df.duplicated(keep=False)])
    final_df.drop_duplicates(inplace=True)
    return final_df, df_test

# pre-process the data to avoid data leakage due to cross-over


# @profile
def data_preprocessor(df_test, df_sales_train):
    # data cleaning : filter out all the rows which have values equal to or less than 0$
    # the deep copy is not necessary but a good practice because slicing yields shallow copy
    df_sales_train = df_sales_train.loc[df_sales_train['item_price'] > 0].copy(
        deep=True)
    # introduce datetime features for monthly forecasting
    df_sales_train['year'] = pd.to_datetime(
        df_sales_train['date']).dt.strftime('%Y')
    df_sales_train['month'] = df_sales_train.date.apply(
        lambda x: datetime.strptime(x, '%d.%m.%Y').strftime('%m'))
    # data leakage handler: to make sure no shops which are not present in the test set to be found in the training set because this is a forecast problem the shops for which we forecast mattter
    shop_id_list = df_test['shop_id'].unique().tolist()
    item_id_list = df_test['item_id'].unique().tolist()
    cl_train = df_sales_train[df_sales_train['shop_id'].isin(shop_id_list)]
    cl_train = cl_train[cl_train['item_id'].isin(item_id_list)]

    print("shape:", df_sales_train.shape, "sample:", df_sales_train.head(5), "column name sales train:",
          df_sales_train.columns.tolist(), "column names for cl_train:", cl_train.columns.tolist())
    # since I don't need any text features I can afford to drop them because they are represented by numbers such shop_id etc
    # when you do inplace=True we can save space by not declaring an extra variable for the df
    # NOTE: the item_cnt_day has values of -1 which is weird and is removed in the cl_train set
    # print(df_sales_train['item_cnt_day'].unique().tolist())
    cl_train.drop(['item_category_name', 'shop_name',
                   'item_name'], axis=1, inplace=True)

    group = cl_train.groupby(
        ['date_block_num', 'shop_id', 'month', 'year', 'item_category_id', 'item_id'], as_index=False).agg({'item_price': ['sum', 'mean'], 'item_cnt_day': ['sum', 'mean', 'count']})
    # DEBUG##print("To rename columns:", cl_train.columns.tolist())
    # To convert from groups to dataframe
    cl_train = pd.DataFrame(group)
    # Rename columns from tuples created earlier from the aggregation
    cl_train.columns = ['date_block_num', 'shop_id',
                        'month', 'year', 'item_category_id', 'item_id', 'item_price', 'mean_item_price', 'item_cnt', 'mean_item_cnt', 'dealings']
    return df_sales_train, cl_train

# Exploratory data analysis for the given dataset
# @profile


def data_analyser(df_sales_train, cl_train):
    # to figure out trends in sales from month to month i.e. to speculate why there is peak like the holidays christmas or thanksgiving etc
    item_cnt_monthly_avg = cl_train.groupby(['month'], as_index=False)[
        'item_cnt'].sum()
    _, axes = plt.subplots(1, 1, figsize=(14, 8))
    # the trend observed here is there is spike in sales at the end of the year
    sns.lineplot(x="month", y="item_cnt",
                 data=item_cnt_monthly_avg).set_title("monthly trends - item count sum total sales")
    # display the graph
    plt.show()

    # the trend observed here is cattegories 9,34 & 71 outsell the rest by a long margin we can -
    # try to understand why by involving the business owners if there is lack of data
    item_categories_avg = cl_train.groupby(
        ['item_category_id'], as_index=False)['item_cnt'].mean()
    _, axes = plt.subplots(1, 1, figsize=(14, 8))
    sns.barplot(x="item_category_id", y="item_cnt", data=item_categories_avg,
                palette="rocket").set_title("monthly trends - item_category mean sales")
    # display the graph
    plt.show()

    item_cnt_sum = cl_train.groupby(['shop_id'], as_index=False)[
        'item_cnt'].sum()
    _, axes = plt.subplots(1, 1, figsize=(14, 8))
    # the trend with shops 31,25,28 outsell most of the others in the group
    sns.barplot(x="shop_id", y="item_cnt", data=item_cnt_sum,
                palette="rocket").set_title("monthly trends - shop_id mean sales")
    sns.jointplot(x="item_cnt", y="item_price", data=cl_train, height=8)
    plt.show()
    plt.subplots(figsize=(14, 8))
    sns.boxplot(cl_train['item_cnt'])
    plt.show()
    # after doing the eda and plotting the values we see that these outliers will skew the model predictions and hence need to be removed
    # Depending on how the model performs we can change these thresholds for outliers
    cl_train = cl_train.loc[(cl_train['item_cnt'] >= 0) & (
        cl_train['item_cnt'] <= 500) & (cl_train['item_price'] < 380000)].copy(deep=True)

    return cl_train


# Add or derive features to help the model have better accuracy based on EDA or the type of problem example: forecasting problem
# We can further add various other features based on how the model is performing such as expanding window, price deltas etc..
# @profile
def feature_engg(cl_train):
    # lag based to predict the next month
    cl_train['item_cnt'] = cl_train['item_cnt'].astype(int)
    cl_train['item_cnt_month'] = cl_train.sort_values(
        'date_block_num').groupby(['shop_id', 'item_id'])['item_cnt'].shift(-1)
    # hypothesis 1: cost of individual items this helps us to understand how much the price of any item influences the prediction
    cl_train['item_price_per_unit'] = cl_train['item_price'].divide(
        cl_train['item_cnt'].where(cl_train['item_cnt'] != 0.0))
    cl_train['item_price_per_unit'].fillna(0.0, inplace=True)
    # hypothesis 2: lag based feature to predict the value at the next time (t+1) given the value at the previous time (t-1).

    for lag in [1, 2, 3, 4]:
        feature_str = ('item_cnt_shifted_%s' % lag)
        cl_train[feature_str] = cl_train.sort_values('date_block_num').groupby(
            ['shop_id', 'item_category_id', 'item_id'])['item_cnt'].shift(lag)
        # Fill the empty shifted features with 0
        cl_train[feature_str].fillna(0, inplace=True)

    # # hypothesis 3: Rolling window based features for a q
    # # Mean value
    # # Similar to this we can add several other aggregators like median, sum etc for features

    def mean_calc(x):
        return x.rolling(window=4, min_periods=1).mean()
    # the pandas apply takes in a function and iterates over the entire df hence the func mean_calc
    cl_train['item_cnt_rolling_mean'] = cl_train.sort_values('date_block_num').groupby(
        ['shop_id', 'item_category_id', 'item_id'])['item_cnt'].apply(mean_calc)

    # Fill the empty mean features with 0
    cl_train['item_cnt_rolling_mean'].fillna(0, inplace=True)
    return cl_train


# @profile
def main():
    t1 = time.time()
    df_sales_train, df_test = data_loader()
    print("after data loader:", df_sales_train.head(),
          df_sales_train.shape, df_sales_train.info)
    df_sales_train, cl_train = data_preprocessor(df_test, df_sales_train)
    print("after data preprocessor:", df_sales_train.shape, cl_train.shape)
    cl_train = data_analyser(df_sales_train, cl_train)
    cl_train = feature_engg(cl_train)
    t2 = time.time()
    print("After feature engineering final dataset:",
          cl_train.shape, cl_train.head(5))
    print("Run for the code:", t2-t1)


if __name__ == '__main__':
    main()
